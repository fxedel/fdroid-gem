Gem::Specification.new do |s|
  s.name        = 'fdroid'
  s.version     = '0.0.1'
  s.date        = '2017-01-11'
  s.summary     = "F-Droid - Free and Open Source Android App Repository"
  s.description = "Browse packages of a F-Droid repository."
  s.authors     = ["Nico Alt"]
  s.email       = 'nicoalt@posteo.org'
  s.files       = ["lib/fdroid.rb"]
  s.homepage    =
    'https://gitlab.com/nicoalt/fdroid-gem'
  s.license       = 'AGPL-3.0'
end
