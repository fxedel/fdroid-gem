# F-Droid Gem

[![Gem Version](https://badge.fury.io/rb/fdroid.svg)](https://badge.fury.io/rb/fdroid)

With this gem you can browse packages of a F-Droid repository.

## License

This program is Free Software:
You can use, study share and improve it at your will.
Specifically you can redistribute and/or modify it under the terms of the
[GNU Affero General Public License](https://www.gnu.org/licenses/agpl.html)
as published by the Free Software Foundation,
either version 3 of the License,
or (at your option) any later version.
